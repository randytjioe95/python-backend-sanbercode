use classicmodels;
SELECT
    o.orderNumber AS 'Order Number',
    o.orderDate AS 'Order Date',
    c.customerName AS 'Customer Name',
    c.city AS 'City',
    c.country AS 'Country',
    od.quantityOrdered AS 'Quantity Ordered',
    p.productName AS 'Product Name'
FROM
    orders o
JOIN
    customers c ON o.customerNumber = c.customerNumber
JOIN
    orderdetails od ON o.orderNumber = od.orderNumber
JOIN
    products p ON od.productCode = p.productCode
WHERE
    p.productName = '1992 Ferrari 360 Spider red'
    AND o.orderDate BETWEEN '2004-08-01' AND '2004-12-01'
ORDER BY
    o.orderDate;