# 1. Buatlah sebuah fungsi untuk membaca dan menampilkan keseluruhan file txt dengan
# input fungsi berupa nama file. #memangil fungsi
# #read_txt(fname)
# #output diharapkan
# # Kata programming mungkin sudah tidak asing lagi di telinga kita. # Programming sangat erat kaitannya dengan industri IT. # Banyak hal yang telah dihasilkan dari programming yang saat ini sudah jadi konsumsi
# kita sehari-hari, seperti produk-produk microsoft, facebook, apple dan lain sebagainya.

def read_txt(fname):
    try:
        with open(fname, 'r') as file:
            content = file.read()
            print(content)
    except FileNotFoundError:
        print("File not found.")

# Memanggil fungsi dengan nama file yang Anda inginkan
read_txt("1.txt")

# 2. Buatlah sebuah fungsi untuk membaca file json yang disediakan dan mengambil data
# "cases" covid di indonesia. return menjadi satu buah list. #memanggil fungsi
# #cases = read_covid()
# #print(cases)
# #output diharapkan
# # [57770,60695, ..., 291182]

import json

def read_covid():
    try:
        with open('2.json', 'r') as file:
            data = json.load(file)
            cases_list = [entry['Cases'] for entry in data]
            return cases_list
    except FileNotFoundError:
        return []

# Memanggil fungsi dan mencetak data cases COVID di Indonesia
cases = read_covid()
print(cases)





