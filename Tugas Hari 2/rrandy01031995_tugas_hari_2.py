# Soal 1:


print("Hello World Python")

# Expected Output:

# Hello World Python

# Soal 2:


# Pertambahan
result_addition = 5 + 3
print("Hasil pertambahan:", result_addition)

# Perkurangan
result_subtraction = 10 - 4
print("Hasil perkurangan:", result_subtraction)

# Perkalian
result_multiplication = 6 * 7
print("Hasil perkalian:", result_multiplication)

# Pembagian
result_division = 20 / 5
print("Hasil pembagian:", result_division)

# Expected Output:


# Hasil pertambahan: 8
# Hasil perkurangan: 6
# Hasil perkalian: 42
# Hasil pembagian: 4.0

# Soal 3:



a = 10
b = 3

integer_result = a // b
float_result = a / b

print("Hasil pembagian (integer):", integer_result)
print("Hasil pembagian (float):", float_result)

# Expected Output:


# Hasil pembagian (integer): 3
# Hasil pembagian (float): 3.3333333333333335

# Soal 4:



firstname = "Afrida"
lastname = "Hafizhatul Ulum"

print("Hello sanbercode, saya", firstname, lastname + "! saya siap belajar python backend development.")

# Expected Output:

# Hello sanbercode, saya Afrida Hafizhatul Ulum! saya siap belajar python backend development.

# Soal 5:


usia = 28
kata = 'usiaku: '
print(kata + str(usia) + ' tahun')

# Expected Output:

# usiaku: 28 tahun
